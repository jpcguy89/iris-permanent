//
//  Patient.m
//  IRIS
//
//  Created by Joseph Cavallaro on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "Patient.h"

@implementation Patient

@synthesize fname;
@synthesize mname;
@synthesize lname;
@synthesize age;
@synthesize weight;
@synthesize height;
@synthesize roomNumber;

@synthesize conditions;
@synthesize history;

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:fname      forKey:@"fname"];
    [coder encodeObject:mname      forKey:@"mname"];
    [coder encodeObject:lname      forKey:@"lname"];    
    [coder encodeObject:age        forKey:@"age"];
    [coder encodeObject:weight     forKey:@"weight"];
    [coder encodeObject:height     forKey:@"height"];
    [coder encodeObject:roomNumber forKey:@"roomNumber"];
    [coder encodeObject:conditions forKey:@"conditions"];
    [coder encodeObject:history    forKey:@"history"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    [self init];
            
            self.fname  = [decoder decodeObjectForKey:@"fname" ];
            self.mname  = [decoder decodeObjectForKey:@"mname" ];
            self.lname  = [decoder decodeObjectForKey:@"lname" ];
              self.age  = [decoder decodeObjectForKey:@"age"   ];
            self.weight = [decoder decodeObjectForKey:@"weight"];
            self.height = [decoder decodeObjectForKey:@"height"];
        self.roomNumber = [decoder decodeObjectForKey:@"roomNumber"];
        self.conditions = [decoder decodeObjectForKey:@"conditions"];
           self.history = [decoder decodeObjectForKey:@"history"];
        
        
        return self;
}

-(id)initWithFullName:(NSString *)first middleName:(NSString*)middle lastName:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
        [self init];
        fname = first;
        mname = middle;
        lname = last;
    }
    
    return self;
}



-(id)initWithFirstLast:(NSString *)first lastName:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
            fname  = first;
            lname  = last;
            mname  = @"";
            lname  = @"";
              age  = @"";
            weight = @"";
            height = @"";
        roomNumber = @"";
    }
    
    return self;
}

-(id)initWithFirst:(NSString *)first
{
    self = [super init];
    
    if(self)
    {
        [self init];
        fname = first;
    }
    
    return self;
}

-(id)initWithLast:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
        [self init];
        lname = last;
    }
    
    return self;
}

-(id)init
{
    self = [super init];
    
    if(self)
    {   
             fname = @"";
             mname = @"";
             lname = @"";
               age = @"";
            weight = @"";
            height = @"";
        roomNumber = @"";
    }
    
    return self;
}

-(id)clear
{
         fname = @"";
         mname = @"";
         lname = @"";
           age = @"";
        weight = @"";
        height = @"";
    roomNumber = @"";
    
    return self;
}

-(NSString*)printAll
{
    NSMutableString* returnString = [NSMutableString stringWithFormat:@"Name: %@\n", [self printFullName]];
    
    if([self.age isEqualToString:@""])
    {
        [returnString appendString:@" Age: not found in the patient's file.\n"];
    }
    else
    { 
        [returnString appendFormat:@" Age: %@\n", self.age];
    }
    
    if([self.weight isEqualToString:@""])
    {
        [returnString appendString:@" Weight: not found in the patient's file.\n"];
    }
    else
    {
        [returnString appendFormat:@" Weight: %@ pounds\n"];
    }
    
    if([self.height isEqualToString:@""])
    {
        [returnString appendString:@" Height: not found in the patient's file.\n"];
    }
    else
    {
        [returnString appendFormat:@" Height: %@\n", self.height];
    }
    
    if([self.roomNumber isEqualToString:@""])
    {
        [returnString appendString:@"Room Number: not found in the patient's file.\n"];
    }
    else 
    {
        [returnString appendFormat:@"Room Number: %@\n"];
    }
    
    if([self.conditions count] == 0)
    {
        [returnString appendString:@" Conditions: No known conditions\n"];
    }
    else 
    {
        [returnString appendFormat:@" Conditions: %@\n", [self printConditions]];
    }
    
    return returnString;
    
    //return [NSString stringWithFormat:@"Name: %@\n Age: %@\n Weight: %@\n Height: %@ \n Room Number: %@\n Conditions: \n%@", [self printFullName], self.mname, self.lname, self.age, self.weight, self.height, self.roomNumber, [self printConditions]];
}

-(NSString*)printFullName
{
    return [NSString stringWithFormat:@"%@ %@ %@", self.fname ,self.mname ,self.lname];
}

-(NSString*)printFirstLast
{
    return [NSString stringWithFormat:@"%@ %@", self.fname ,self.lname];
}

-(NSString*)printConditions
{
    return [[conditions valueForKey:@"description"] componentsJoinedByString:@"\n"];
}

@end
