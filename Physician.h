//
//  Physician.h
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*! 
 *  \brief     Physician Object Class
 *  \details   Creates Physician objects and Stores relevant information for them.
 *  \author    Joe Cavallaro
 *  \date      March-April 2012
 *  \copyright Joseph Peter Cavallaro 2012
 */
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Address.h"

@interface Physician : NSObject <NSCoding>

//@property (nonatomic, retain) Address* facilityAddress;
//@property (nonatomic, retain) Address* officeAddress;

@property (nonatomic, retain) NSMutableArray* specialties;
@property (nonatomic, retain) NSMutableArray* subSpecialties;
@property (nonatomic, retain) NSMutableArray* acceptedInsurance;

@property (nonatomic, retain) NSString* fname;
@property (nonatomic, retain) NSString* mname;
@property (nonatomic, retain) NSString* lname;

@property (nonatomic, retain) NSString* hospitalAffiliation;
@property (nonatomic, retain) NSString* facilityName;
@property (nonatomic, retain) NSString* officeName;
@property (nonatomic, retain) NSString* phone_cell;
@property (nonatomic, retain) NSString* phone_emerg;
@property (nonatomic, retain) NSString* phone_office;

- (NSString*)printFirstLast;
- (NSString*)printFullName;
- (NSString*)printAll;

- (id)initWithFullName :(NSString*)first middleName:(NSString*) middle lastName:(NSString*) last;
- (id)initWithFirstLast:(NSString*)first   lastName:(NSString*) last;
- (id)initWithFirst    :(NSString*)first;
- (id)initWithLast     :(NSString*)last;
- (id)init;


//NSCoding Required Protocol Methods
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
@end
