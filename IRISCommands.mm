//
//  IRISCommands.mm
//  IRISSnippet
//
//  Created by  on 12/29/11.
//  Copyright (c) 2011 .me. All rights reserved.
//

#import "IRISCommands.h"
#import "SiriObjects.h"
#import "Patient.h"
#import "Physician.h"


@implementation IRISCommands

@synthesize currentPerson;
@synthesize patients;
@synthesize physicians;
@synthesize allConditions;
@synthesize allSpecialties;


BOOL loaded = NO; /**> "Loaded" ensures that actions are not performed on the Patient/Physician at index "currentPerson" if the user has not loaded a Patient/Physician. */
BOOL docSwitch = NO; /**> If YES, Indicates that the currently loaded "person" is a doctor, not a patient. */

//Methods Required by NSCoding
- (void)encodeWithCoder:(NSCoder*)coder
{
    [coder encodeObject:patients   forKey:@"patients"  ];
    [coder encodeObject:physicians forKey:@"physicians"];
}

- (id)initWithCoder:(NSCoder*)decoder
{
    if(self = [super init])
    {
        [self setPatients:  [decoder decodeObjectForKey:@"patients"  ]];
        [self setPhysicians:[decoder decodeObjectForKey:@"physicians"]];
    }
    
    return self;
}
//END of methods required by NSCoding


- (NSString *) pathForDataFile /**> Creates file path where patient list ("patients") will be saved. */
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *folder = @"~/Library/Application Support/IRIS/";
    folder = [folder stringByExpandingTildeInPath];
    
    NSError** error = nil;
    
    if ([fileManager fileExistsAtPath: folder] == NO)
    {
        [fileManager createDirectoryAtPath:folder withIntermediateDirectories:NO attributes:nil error:error];
    }
    
    NSString *fileName = @"patientlist";
    
    return [folder stringByAppendingPathComponent: fileName];    
}
//END file path creation

- (id)init
{
    [super init];
    
    if(self)
    {
        patients   = [[NSMutableArray alloc] init];
        physicians = [[NSMutableArray alloc] init];
        
        //list of conditions I took from http://www.cureresearch.com/lists/condsaz.htm to check against user-input to ensure only valid entries/conditions are stored. 
        allConditions = [[NSArray alloc] initWithObjects:@"aids", @"allergies", @"alzheimer's disease", @"anxiety disorder", @"arthritis", 
                         @"asthma", @"astigmatism", @"autoimmune disease", @"benign prostate hyperplasia", @"bph", @"bipolar disorder", 
                         @"manic depressive disorder", @"brain cancer", @"breast cancer", @"cancer candidiasis", @"cataracts", @"celiac disease", 
                         @"cervical cancer", @"chicken pox", @"chlamydia", @"chronic fatigue syndrome", @"cfs", @"chronic illness", @"cold sores", 
                         @"colon cancer", @"constipation", @"common cold", @"copd", @"cough", @"crohn's disease", @"cystic fibrosis", @"dementia", 
                         @"diabetes", @"diarrhea", @"depression", @"eczema", @"endometriosis", @"eye disorder", @"fibroids", @"fibromyalgia", @"flu", 
                         @"influenza", @"food poisoning", @"gallstones", @"genital herpes", @"gonorrhea", @"graves' disease", @"hashimoto's thyroiditis", 
                         @"hay fever", @"headache", @"heart disease", @"hemochromatosis", @"hepatitis", @"herpes", @"high blood pressure", @"high cholesterol", 
                         @"hiv", @"hodgkin's disease", @"hpv", @"human papilloma virus", @"hypertension", @"hypopituitarism" @"impotence", @"insomnia", 
                         @"irritable bowel syndrome", @"jaundice", @"kidney disease", @"lactose intolerance", @"leukemia", @"liver cancer", @"liver disease", 
                         @"lung cancer", @"lupus", @"Lyme disease", @"lymphoma", @"meningitis", @"meningococcal disease", @"menopause", @"mental illness", 
                         @"myopia", @"short-sightedness", @"migraine", @"multiple sclerosis", @"muscular dystrophy", @"narcolepsy", @"non-hodgkin's lymphoma", 
                         @"obesity", @"osteoporosis", @"otitis media", @"middle ear infection", @"ovarian cancer", @"overweight", @"pain", @"parkinson's disease", 
                         @"pelvic inflammatory disease", @"pertussis", @"pregnancy", @"premenstrual syndrome", @"pms", @"prostate cancer", @"prostate disorder", 
                         @"raynaud's phenomenon", @"sars", @"sexually transmitted diseases", @"sleep disorder", @"smoking", @"stroke", @"thrush", @"thyroid disorder", 
                         @"whooping cough", nil];
    }
    
    return self;    
}

- (void)dealloc
{
	NSLog(@"IRISCommands dealloc");
    
	[super dealloc];
}



- (BOOL)handleSpeech:(NSString*)text tokens:(NSArray*)tokens tokenSet:(NSSet *)tokenset context:(id<SEContext>)ctx
{
    tokens = [[tokens mutableCopy] autorelease]; // allows for inserting/removing of words if needed.
    tokenset = [[tokenset mutableCopy] autorelease];// ""
    
    static BOOL asked = FALSE;
	NSLog(@">> IRISCommands handleSpeech %@", text);
    
    
    if([tokens count] <= 2 && ([tokenset containsObject:@"add"] || [tokenset containsObject:@"new"] || [tokenset containsObject:@"create"]) && [tokenset containsObject:@"patient"] && !([tokenset containsObject:@"condition"])) /**> Asks the user for the first and last name of the Patient they want to add, then creates the Patient object and adds it to 'patients'. */
    {
        if(!asked)
		{
			asked = YES; // store so we know next time this method is called

            SOObject* askView = [ctx createAssistantUtteranceView:@"What are the patient's first and last names?"];
			[[askView objectForKey:@"properties"] setObject:[NSNumber numberWithBool:YES] forKey:@"listenAfterSpeaking"];
			NSArray* views = [NSArray arrayWithObject:askView];
            
            [ctx sendAddViews:views];
            
            [ctx sendRequestCompleted];
            return YES;
		}
	}
    
    if (asked)
    {
        asked = NO; // reset...
        
        Patient* p = [[Patient alloc] init];
        
        [p setFname:[tokens objectAtIndex:0]];
        [p setLname:[tokens objectAtIndex:1]];
        
        [patients addObject:p];
        
        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the patient %@ %@ to the system.", [tokens objectAtIndex:0], [tokens objectAtIndex:1]]];
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    else if([tokens count] <= 2 && ([tokenset containsObject:@"add"] || [tokenset containsObject:@"new"]) && ([tokenset containsObject:@"physician"] || [tokenset containsObject:@"doctor"]) && !([tokenset containsObject:@"condition"])) /**> Asks the user for the first and last name of the Physician they want to add, then creates the Physician object and adds it to 'physicians'. */
    {
        if(!asked)
		{
			asked = YES; // store so we know next time this method is called
            
            SOObject* askView = [ctx createAssistantUtteranceView:@"What are the patient's first and last names?"];
			[[askView objectForKey:@"properties"] setObject:[NSNumber numberWithBool:YES] forKey:@"listenAfterSpeaking"];
			NSArray* views = [NSArray arrayWithObject:askView];
            
            [ctx sendAddViews:views];
            
            [ctx sendRequestCompleted];
            return YES;
		}
	}
    
    if (asked)
    {
        asked = NO; // reset...
        
        Physician* p = [[Physician alloc] init];
        
        [p setFname:[tokens objectAtIndex:0]];
        [p setLname:[tokens objectAtIndex:1]];
        
        [physicians addObject:p];
        
        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added Doctor %@ %@ to the system.", [[physicians lastObject] fname], [[physicians lastObject] lname] ]];
        
        [ctx sendRequestCompleted];
        return YES;
    }

    if(([tokenset containsObject:@"add"] || [tokenset containsObject:@"create"] || [tokenset containsObject:@"new"]) && ([tokenset containsObject:@"patient"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"doctor"]) && [tokens count] >= 3 && [tokens count] <= 6 && !([tokenset containsObject:@"condition"] ||[tokenset containsObject:@"medical"])) /**> A one call command to create a Patient or Physician. Uses the user supplied first & last name, or first, middle, and last name, to create either a Patient or Physician object, and then stores it either in 'patients or 'physicians'. */
    {
        if([tokenset containsObject:@"patient"])
        {
            Patient* patient = [[Patient alloc] init];
            
            int indexOfPatient = [tokens indexOfObject:@"patient"];
            int count = [tokens count];
            
            count -= 1; //count is 1 higher than indexes, skews result. Subtract 1 to "equalize".
            
            int numberOfNames = count - indexOfPatient;
            
            int first;
            int middle;
            int last;
            
            if(numberOfNames == 2)
            {
                first = indexOfPatient + 1;
                last  = indexOfPatient + 2;
                
                NSLog(@"Setting fname...fname to: %@", [tokens objectAtIndex:first]);
                [patient setFname:[tokens objectAtIndex:first]];
                
                NSLog(@"Setting fname...lname to: %@", [tokens objectAtIndex:last]);
                [patient setLname:[tokens objectAtIndex:last]];
                
                [patients addObject:patient];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the patient %@ %@ to the system.", [tokens objectAtIndex:2], [tokens objectAtIndex:3]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(numberOfNames == 3)
            {
                first  = indexOfPatient + 1;
                middle = indexOfPatient + 2;
                last   = indexOfPatient + 3;
                
                NSLog(@"Setting fname...fname to: %@", [tokens objectAtIndex:first]);
                [patient setFname:[tokens objectAtIndex:first]];
    
                NSLog(@"Setting mname...mname to: %@", [tokens objectAtIndex:middle]);
                [patient setMname:[tokens objectAtIndex:middle]];
                
                NSLog(@"Setting lname...lname to: %@", [tokens objectAtIndex:last]);
                [patient setLname:[tokens objectAtIndex:last]];
                
                NSLog(@"Adding this patient: %@", [patient printFullName]);
                [patients addObject:patient];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the patient %@ %@ %@ to the system.", [[patients lastObject] fname], [[patients lastObject] mname], [[patients lastObject] lname]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        
        else if([tokenset containsObject:@"physician"] || [tokenset containsObject:@"doctor"] || [tokenset containsObject:@"Doctor"])
        {
            NSLog(@"Initializing Physician object \"physician\"");
            Physician* physician = [[Physician alloc] init];
            
            int indexOfPhysician;
            
            if([tokenset containsObject:@"physician"])
            {
                indexOfPhysician = [tokens indexOfObject:@"physician"];
            }
            
            else if ([tokenset containsObject:@"doctor"])
            {
                indexOfPhysician = [tokens indexOfObject:@"doctor"];
            }
            
            else if([tokenset containsObject:@"dr."])
            {
                indexOfPhysician = [tokens indexOfObject:@"dr."];
            }
            int count = [tokens count];
            
            count -= 1; //count is 1 higher than indexes, skews result. Subtract 1 to "equalize".
            
            int numberOfNames = count - indexOfPhysician;
            
            int first;
            int middle;
            int last;
            
            if(numberOfNames == 2)
            {
                
                first  = indexOfPhysician + 1;
                last   = indexOfPhysician + 2;
                
                NSLog(@"Setting fname...fname to: %@", [tokens objectAtIndex:first]);
                [physician setFname:[tokens objectAtIndex:first]];
                
                NSLog(@"Setting fname...lname to: %@", [tokens objectAtIndex:last]);
                [physician setLname:[tokens objectAtIndex:last]];
                
                
                [physicians addObject:physician];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added Doctor %@ %@ to the system.", [[physicians lastObject] fname], [[physicians lastObject] lname]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(numberOfNames == 3)
            {

                first  = indexOfPhysician + 1;
                middle = indexOfPhysician + 2;                
                last   = indexOfPhysician + 3;
                
                NSLog(@"Setting fname...fname to: %@", [tokens objectAtIndex:first]);
                [physician setFname:[tokens objectAtIndex:first]];
            
                NSLog(@"Setting fname...mname to: %@", [tokens objectAtIndex:middle]);
                [physician setMname:[tokens objectAtIndex:middle]];
            
                NSLog(@"Setting fname...lname to: %@", [tokens objectAtIndex:last]);
                [physician setLname:[tokens objectAtIndex:last]];
                
                [physicians addObject:physician];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added Doctor %@ %@ to the system.", [[physicians lastObject] fname], [[physicians lastObject] mname], [[physicians lastObject] lname]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        
        //Assume user is asking to create a new patient
        else if(!([tokenset containsObject:@"patient"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"doctor"] || [tokenset containsObject:@"dr."]))
        {
            Patient* patient = [[Patient alloc] init];
            
            int indexOfWord;
            
            if([tokenset containsObject:@"add"])
            {
                indexOfWord = [tokens indexOfObject:@"add"];
            }
            
            else if([tokenset containsObject:@"create"])
            {
                indexOfWord = [tokens indexOfObject:@"create"];
            }
            
            else if([tokenset containsObject:@"new"])
            {
                indexOfWord = [tokens indexOfObject:@"new"];
            }
            
            int count = [tokens count];
            count -= 1; //count is 1 higher than indexes, skews result. Subtract 1 to "equalize".
            
            int numberOfNames = count - indexOfWord;
            
            int first;
            int middle;
            int last;
            
            if(numberOfNames == 2)
            {

                first = indexOfWord + 1;
                last  = indexOfWord + 2;  
                
                NSLog(@"Setting fname to: %@", [tokens objectAtIndex:first]);
                [patient setFname:[tokens objectAtIndex:first]];

                NSLog(@"Setting lname to: %@", [tokens objectAtIndex:last]);
                [patient setLname:[tokens objectAtIndex:last]];
               
                NSLog(@"Adding this patient: \n %@", [patient printFullName]);
                [patients addObject:patient];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added patient %@ %@ to the system.", [[patients lastObject] fname], [[patients lastObject] lname]]];
            }
            
            else if(numberOfNames == 3)
            {

                first  = indexOfWord + 1;
                middle = indexOfWord + 2; 
                last   = indexOfWord + 3;                
                NSLog(@"Setting fname...fname to: %@", [tokens objectAtIndex:first]);
                [patient setFname:[tokens objectAtIndex:first]];
                
                NSLog(@"Setting fname...mname to: %@", [tokens objectAtIndex:middle]);
                [patient setMname:[tokens objectAtIndex:middle]];
                
                NSLog(@"Setting fname...lname to: %@", [tokens objectAtIndex:last]);
                [patient setLname:[tokens objectAtIndex:last]];
                
                NSLog(@"Adding this patient: \n %@", [patient printFullName]);
                [patients addObject:patient];
                
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added patient %@ %@ %@ to the system.", [[patients lastObject] fname], [[patients lastObject] mname], [[patients lastObject] lname]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
    }
    
    //Tuesday 4.3.12
    //Adding a way to enter conditions and medical history to patient files
    //No idea Why the user input "add condition",  then system asking for name of condition and checking that input doesn't work...implementing alternative solution based on testing/debugging results...
    else if([tokenset containsObject:@"add"] && ([tokenset containsObject:@"condition"] && !([tokenset containsObject:@"medical"] && [tokenset containsObject:@"history"]))) /**> Checks to see if the desired condition to be added is valid by cross-checking it with 'allConditions' and if it's valid, adds it to the patient that's currently loaded.*/
    {    
        if(docSwitch == YES)
        {
            [ctx sendAddViewsUtteranceView:@"Oops, it looks like a Physician is currently loaded."];
            
            [ctx sendRequestCompleted];
            return YES;
        }
        else if(!asked)
        {
            asked = YES;
            SOObject* askView = [ctx createAssistantUtteranceView:@"What condition should I add?"];
            [[askView objectForKey:@"properties"] setObject:[NSNumber numberWithBool:YES] forKey:@"listenAfterSpeaking"];
            NSArray* views = [NSArray arrayWithObject:askView];
            [ctx sendAddViews:views];
            
            [ctx sendRequestCompleted];
            return YES;
        }        
    }
    if(asked)
    {
        asked = NO;
        if(loaded)
        {
            if([allConditions containsObject:text])
            {
                [[[patients objectAtIndex:currentPerson] conditions] addObject:text];
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the condition %@ to the patient %@", text, [[patients objectAtIndex:currentPerson] printFirstLast]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else
            {
                [ctx sendAddViewsUtteranceView:@"That doesn't appear to be a valid condition."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        else if(!loaded)
        {
            [ctx sendAddViewsUtteranceView:@"Oops! It appears you forgot to load a patient file first. Please load a patient file and then try your request again."];
            
            [ctx sendRequestCompleted];
            return YES;
        }
    }
    
    
    //Saturday 4.14.12
    //This DOES work...
    if(([tokenset containsObject:@"add"] || [[tokens objectAtIndex:0] isEqual:@"new"]) && [tokenset containsObject:@"condition"] && [tokens count] >2 && !([tokenset containsObject:@"medical"] && [tokenset containsObject:@"history"])) /**> A one-call method to add a condition to a patient. Checks it against 'allConditions' to ensure it is a real, valid medical condition. If it's valid, adds it to the currently loaded patient.*/
    {
        if(docSwitch == YES)
        {
            [ctx sendAddViewsUtteranceView:@"Oops, it looks like a Physician is currently loaded."];
            
            [ctx sendRequestCompleted];
            return YES;
        }
        if([allConditions containsObject:text])
        {
            [[[patients objectAtIndex:currentPerson] conditions] addObject:text];
            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the condition %@ to the patient %@", text, [[patients objectAtIndex:currentPerson] printFirstLast]]];
            
            [ctx sendRequestCompleted];
            return YES;
        }
        
        NSString* newString;
        
        if([tokens count] == 3)
        {
            if([allConditions containsObject:[tokens objectAtIndex:2]])
            {
                [[[patients objectAtIndex:currentPerson] conditions] addObject:[tokens objectAtIndex:2]];
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the condition \"%@\" for the patient %@", [tokens objectAtIndex:2], [[patients objectAtIndex:currentPerson] printFirstLast]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            else
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, that's not a valid condition. Please try again."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        
        else if([tokens count] == 4)
        {
            newString = [NSString stringWithFormat:@"%@ %@", [tokens objectAtIndex:2], [tokens objectAtIndex:3]];
            
            if([allConditions containsObject:newString])
            {
                [[[patients objectAtIndex:currentPerson] conditions] addObject:newString];
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the condition \"%@\" for the patient %@", newString, [[patients objectAtIndex:currentPerson] printFirstLast]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            else
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, that's not a valid condition. Please try again."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        
        else if([tokens count] == 5)
        {
            newString = [NSString stringWithFormat:@"%@ %@ %@", [tokens objectAtIndex:2],[tokens objectAtIndex:3],[tokens objectAtIndex:4]];
            
            if([allConditions containsObject:newString])
            {
                [[[patients objectAtIndex:currentPerson] conditions] addObject:newString];
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added the condition \"%@\" for the patient %@", newString, [[patients objectAtIndex:currentPerson] printFirstLast]]];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else 
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, that's not a valid condition. Please try again."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }
        
        else
        {
            [ctx sendAddViewsUtteranceView:@"Sorry, that's not a valid condition. Please try again."];
            
            [ctx sendRequestCompleted];
            return YES;
        }
    }
    
    /*
     Copies the index of a user-selected Patient file (it lists all of their names)
     into 'currentPerson'. This allows the selected Patient file to be edited.
     */
    
    //EDIT: Tuesday 4-3-12
    //-Re-implemented this method, using patient's name rather than asking for a corresponding number.    
    //- Also added an if statement to an respond and quit if user gives an invalid (not a first or last name) response.
    
    //This process is simplified in the next "load" if-statement, which allows the user to say, for example "load patient john doe", or "load john doe"
    
    
    
    else if([tokenset containsObject:@"edit"] && [tokens count] < 3) /**> Asks the user for the name of the Patient or Physician they want to load (for editing). Then checks the list of 'patients' and 'physicians' to see if any of the objects inside of them have the same name as the user is searching for. If one is found, it's index is stored in 'currentPerson'. If not, the user is informed that no match was found, and the system is returned to it's waiting state. (NOTE: for this method, it is assumed that the user is searching for a Patient, as I don't know of a way to have the system get both a name and a choice between Patient and Physician in the same query.) */
    {
        NSLog(@"asked: %@ | token count: %i | # of Patients in \"patients\": %i", asked ? "YES" : "NO", [tokens count], [patients count]);
        if(!asked)
        {
            asked = YES; NSLog(@"asked should = YES: %@", asked);
            NSEnumerator* patientsEnm = [patients objectEnumerator];
            Patient* p = [[Patient alloc] init];
            
            SOObject* askView = [ctx createAssistantUtteranceView:@"After the list of patient names, please say the name of the patient whose file you want to load."];
            [[askView objectForKey:@"properties"] setObject:[NSNumber numberWithBool:YES] forKey:@"listenAfterSpeaking"];
            NSArray* views = [NSArray arrayWithObject:askView];
            [ctx sendAddViews:views];
            
            
            while(p = [patientsEnm nextObject])
            {
                NSLog(@"Is p == nil?: %@", [p isEqual:nil] ? "YES" : "NO");
                if([[p mname] isEqual:@""])
                {
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@ %@ %@", [p fname], [p mname], [p lname]]];
                }   
            }
            
            [ctx sendAddViewsUtteranceView:@"Please say the patient's first and last name or their first, middle and last name to edit that patient's file."];
            
            [ctx sendRequestCompleted];
            return YES;
        }
    }
        
    if(asked)
    {
        asked = NO;
        
        NSEnumerator* patientsEnm = [patients objectEnumerator];
        Patient* p = [[Patient alloc] init];
        
        while(p = [patientsEnm nextObject])
        {
            if([tokens count] == 2)
            { 
                NSLog(@"");
                if([[p fname] isEqualToString:[tokens objectAtIndex:0]] && [[p lname] isEqualToString:[tokens objectAtIndex:1]])
                {
                    currentPerson = [patients indexOfObject:p];
                }
            }
            
            else if([tokens count] == 3)
            {
                if([[p fname] isEqualToString:[tokens objectAtIndex:0]] && [[p mname] isEqualToString:[tokens objectAtIndex:1]] && [[p lname] isEqualToString:[tokens objectAtIndex:2]])
                {
                    currentPerson = [patients indexOfObject:p];
                }
            }
        }
        
        [ctx sendAddViewsUtteranceView:@"No match found."];
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    else if([tokenset containsObject:@"edit"]) /**> Checks the list of 'patients' and 'physicians' to see if any of the objects inside of them have the same name as the user is searching for. If one is found, it's index is stored in 'currentPerson'. If not, the user is informed that no match was found, and the system is returned to it's waiting state. */
    {
        NSEnumerator* patientsEnm = [patients objectEnumerator];
        Patient* p = [[Patient alloc] init];
        
        int count = [tokens count];
        int indexOfPatient = [tokens indexOfObject:@"patient"];
        count -= 1;
        
        int numOfNames = count - indexOfPatient;
        
        if([tokenset containsObject:@"patient"])
        {
            if(numOfNames < 2)
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, in order to load a patient using the patient's name, I need at least the patient's first and last name."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(numOfNames == 2)
            {
                int firstInt = indexOfPatient + 1;
                int lastInt  = indexOfPatient + 2;
                
                NSString* first = [tokens objectAtIndex:firstInt];
                NSString* last  = [tokens objectAtIndex: lastInt];
                
                while(p = [patientsEnm nextObject])
                {
                    if([[p fname] isEqualToString:first] && [[p lname] isEqual:last])
                    {
                        currentPerson = [patients indexOfObject:p];
                        loaded    = YES;
                        docSwitch =  NO;
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the patient %@.", [p printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            else if(numOfNames == 3)
            {
                int firstInt = indexOfPatient  + 1;
                int middleInt = indexOfPatient + 2;
                int lastInt   = indexOfPatient + 3;
                
                NSString* first  = [tokens objectAtIndex:firstInt];
                NSString* middle = [tokens objectAtIndex:middleInt];
                NSString* last   = [tokens objectAtIndex:lastInt];
                
                while(p = [patientsEnm nextObject])
                {
                    if([[p fname] isEqual:first] && [[p mname] isEqual:middle] && [[p lname] isEqual:last])
                    {
                        currentPerson = [patients indexOfObject:p];
                        loaded = YES;
                        docSwitch     = NO;
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the patient %@.", [p printFullName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
        
        else if([tokenset containsObject:@"doctor"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"dr."])
        {
            NSEnumerator* physEnm = [physicians objectEnumerator];
            Physician* doc = [[Physician alloc] init];
            
            int indexOfDoc;
            int count = [tokens count];
            count -= 1;
            
            if([tokenset containsObject:@"doctor"])
            {
                indexOfDoc = [tokens indexOfObject:@"doctor"];
            }
            
            else if([tokenset containsObject:@"physician"])
            {
                indexOfDoc = [tokens indexOfObject:@"physician"];
            }
            
            numOfNames = count - indexOfDoc;
            
            int firstInt;
            int middleInt;
            int lastInt;
            
            NSString* first;
            NSString* middle;
            NSString* last;
            
            if(numOfNames == 2)
            {
                firstInt  = indexOfDoc + 1;
                lastInt   = indexOfDoc + 2;
                
                first  = [tokens objectAtIndex:firstInt];
                last   = [tokens objectAtIndex:lastInt];
                
                while(doc = [physEnm nextObject])
                {
                    if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                    {
                        loaded = YES;
                        docSwitch = YES;
                        currentPerson = [physicians indexOfObject:doc];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the doctor %@.", [doc printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            else if(numOfNames == 3)
            {
                firstInt  = indexOfDoc + 1;
                middleInt = indexOfDoc + 2;
                lastInt   = indexOfDoc + 3;
                
                first  = [tokens objectAtIndex:firstInt];
                middle = [tokens objectAtIndex:middleInt];
                last   = [tokens objectAtIndex:lastInt];
                
                while(doc = [physEnm nextObject])
                {
                    if([[doc fname] isEqualToString:first] && [[doc mname] isEqualToString:middle] && [[doc lname] isEqualToString:last])
                    {
                        loaded = YES;
                        docSwitch = YES;
                        
                        currentPerson = [physicians indexOfObject:doc];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the doctor %@.", [doc printFullName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }

        else if(![tokenset containsObject:@"patient"] || [tokenset containsObject:@"doctor"] || [tokenset containsObject:@"physician"])
        {
            int indexOfEdit = [tokens indexOfObject:@"edit"];
            int count = [tokens count];
            count -= 1;
            int numOfNames = count - indexOfEdit;
            
            if(numOfNames < 2)
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, in order to load a patient using the patient's name, I need at least the patient's first and last name."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(numOfNames == 2)
            {
                int firstInt  = indexOfEdit + 1;
                int lastInt   = indexOfEdit + 2;
                
                NSString* first  = [tokens objectAtIndex:firstInt];
                NSString* last   = [tokens objectAtIndex:lastInt];
                
                while(p = [patientsEnm nextObject])
                {
                    if([[p fname] isEqualToString:first] && [[p lname] isEqualToString:last])
                    {
                        currentPerson = [patients indexOfObject:p];
                        
                        loaded = YES;
                        docSwitch = NO;
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the patient %@.", [p printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            else if([tokens count] == 4)
            {
                while(p = [patientsEnm nextObject])
                {
                    if([[p fname] isEqual:[tokens objectAtIndex:1]] && [[p mname] isEqual:[tokens objectAtIndex:2]] && [[p lname] isEqual:[tokens objectAtIndex:3]])
                    {
                        currentPerson = [patients indexOfObject:p];
                        
                        loaded = YES;
                        docSwitch = NO;
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've loaded the patient %@.", [p printFullName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
    }

    /**! One-call changes/sets for specific variables within a Patient or Physician object to the user provided value. */
    else if(([tokenset containsObject:@"change"]        || [tokenset containsObject:@"set"])      || (([tokenset containsObject:@"add"] && 
            ([tokenset containsObject:@"insurance"]     || [tokenset containsObject:@"specialty"] ||   [tokenset containsObject:@"sub"] || 
             [tokenset containsObject:@"sub-specialty"] || [tokenset containsObject:@"subspecialty"])))) 
    {
        
        if([tokenset containsObject:@"to"])
        {
            int indexOfTo = [tokens indexOfObject:@"to"];
            indexOfTo +=1;
            
            if([tokenset containsObject:@"name"])
            {
                if([tokenset containsObject:@"name"])
                {
                    if([tokenset containsObject:@"first"])
                    {
                        [[patients objectAtIndex:currentPerson] setFname:[tokens objectAtIndex:indexOfTo]];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's first name to %@.", [[patients objectAtIndex:currentPerson] fname]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else if([tokenset containsObject:@"middle"])
                    {
                        [[patients objectAtIndex:currentPerson] setMname:[tokens objectAtIndex:indexOfTo]];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's middle name to %@", [[patients objectAtIndex:currentPerson] mname]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else if([tokenset containsObject:@"last"])
                    {
                        [[patients objectAtIndex:currentPerson] setLname:[tokens objectAtIndex:indexOfTo]];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's last name to %@", [[patients objectAtIndex:currentPerson] lname]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            if(docSwitch == NO)
            {
                if([tokenset containsObject:@"age"])
                {
                    [[patients objectAtIndex:currentPerson] setAge:[tokens objectAtIndex:indexOfTo]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's age to %@", [[patients objectAtIndex:currentPerson] age]]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"weight"])
                {
                    [[patients objectAtIndex:currentPerson] setWeight:[tokens objectAtIndex:indexOfTo]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's weight to %@", [[patients objectAtIndex:currentPerson] weight]]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"height"])
                {
                    [[patients objectAtIndex:currentPerson] setHeight:[tokens objectAtIndex:indexOfTo]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's height to %@", [[patients objectAtIndex:currentPerson] height]]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"room"])
                {
                    [[patients objectAtIndex:currentPerson] setRoomNumber:[tokens objectAtIndex:indexOfTo]];
                        
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's room number to %@", [[patients objectAtIndex:currentPerson] roomNumber]]];
                        
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
            
            if(docSwitch == YES)
            {
                if([tokens count] > 5)
                {
                    NSLog(@"Text: %@\n", text);
                    [ctx sendAddViewsUtteranceView:@"That command is going to be too difficult for me to handle. Try structuring your command like this: \"Add 'Neurology' to 'Specialties'\""];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
               
                if([tokenset containsObject:@"specialty"])
                {
                    int indexOfSpecialty = [tokens indexOfObject:@"specialty"];
                    int indexOfValue     = indexOfSpecialty + 1;
                    
                    [[[physicians objectAtIndex:currentPerson] specialties] addObject:[tokens objectAtIndex:indexOfValue]];
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added %@ to Doctor %@'s specialties.", [[[physicians objectAtIndex:currentPerson] specialties] lastObject], [[physicians objectAtIndex:currentPerson] printFirstLast] ]];
                    
                    [ctx sendRequestCompleted];
                    return YES;  
                }
                
                else if([tokenset containsObject:@"sub-specialty"] || [tokenset containsObject:@"subspecialty"] || [tokenset containsObject:@"sub"])
                {
                    int indexOfSubSpecialty;
                    
                    if([tokenset containsObject:@"sub-specialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"sub-specialty"];
                    }
                    
                    else if([tokenset containsObject:@"subspecialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"subspecialty"];
                    }
                    
                    else if([tokenset containsObject:@"specialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"specialty"];
                    }
                    
                    int indexOfValue = indexOfSubSpecialty + 1;
                    
                    [[[physicians objectAtIndex:currentPerson] subSpecialties] addObject:[tokens objectAtIndex:indexOfValue]];
                    [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s sub-specialties."];
                    
                    [ctx sendRequestCompleted];
                    return YES;  
                }
                
                else if(([tokenset containsObject:@"accepted"] && [tokenset containsObject:@"insurance"]) || [tokenset containsObject:@"insurance"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"hospital"] && [tokenset containsObject:@"affiliation"]) || [tokenset containsObject:@"hospital"] || [tokenset containsObject:@"affiliation"]) 
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"facility"] && [tokenset containsObject:@"name"]) || [tokenset containsObject:@"facility"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"office"] && [tokenset containsObject:@"name"]))
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"emergency"] && [tokenset containsObject:@"phone"]) || [tokenset containsObject:@"emergency"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"cell"] && [tokenset containsObject:@"phone"]) || [tokenset containsObject:@"cell"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if([tokenset containsObject:@"office"] && [tokenset containsObject:@"phone"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if([tokenset containsObject:@"office"] && !([tokenset containsObject:@"phone"] || [tokenset containsObject:@"name"]))
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    if([tokenset containsObject:@"change"])
                    {
                        [ctx sendAddViewsUtteranceView:@"Sorry, I'm not sure if you wanted to change the physician's office name, or office phone number."];
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    else if([tokenset containsObject:@"set"])
                    {
                        [ctx sendAddViewsUtteranceView:@"Sorry, I'm not sure if you wanted to set the physician's office name, or office phone number."];
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
            
        else if(![tokenset containsObject:@"to"])
        {
            if([tokenset containsObject:@"name"])
            {
                int indexOfName = [tokens indexOfObject:@"name"];
                indexOfName += 1;
                
                if([tokenset containsObject:@"first"])
                {
                    [[patients objectAtIndex:currentPerson] setFname:[tokens objectAtIndex:indexOfName]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's first name to %@", [[patients objectAtIndex:currentPerson] fname]]];
                    [ctx sendRequestCompleted];
                    
                    return YES;
                }
                
                else if([tokenset containsObject:@"middle"])
                {
                    [[patients objectAtIndex:currentPerson] setMname:[tokens objectAtIndex:indexOfName]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's middle name to %@", [[patients objectAtIndex:currentPerson] mname]]];
                    [ctx sendRequestCompleted];
                    
                    return YES;
                }
                
                else if([tokenset containsObject:@"last"])
                {
                    [[patients objectAtIndex:currentPerson] setLname:[tokens objectAtIndex:indexOfName]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's last name to %@", [[patients objectAtIndex:currentPerson] lname]]];
                    [ctx sendRequestCompleted];
                    
                    return YES;
                }
            }
            
            if(docSwitch == NO)
            {
                if([tokenset containsObject:@"age"])
                {
                    int indexOfAge = [tokens indexOfObject:@"age"]; //index of the word "age" in 'tokens'.
                    indexOfAge += 1;
                    
                    [[patients objectAtIndex:currentPerson] setAge:[tokens objectAtIndex:indexOfAge]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's age to %@", [[patients objectAtIndex:currentPerson] age]]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"weight"])
                {
                    int indexOfWeight = [tokens indexOfObject:@"weight"]; //index of the word "weight" in 'tokens'.
                    indexOfWeight += 1;
                    
                    [[patients objectAtIndex:currentPerson] setWeight:[tokens objectAtIndex:indexOfWeight]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's weight to %@", [[patients objectAtIndex:currentPerson] weight]]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"height"])
                {
                    int indexOfHeight = [tokens indexOfObject:@"height"]; //index of the word "height" in 'tokens'.
                    indexOfHeight += 1;
                    
                    [[patients objectAtIndex:currentPerson] setHeight:[tokens objectAtIndex:indexOfHeight]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's height to %@", [[patients objectAtIndex:currentPerson] height]]];
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokenset containsObject:@"room"])
                {
                    if([tokenset containsObject:@"number"])
                    {
                        int indexOfNumber = [tokens indexOfObject:@"number"]; //index of the word "number" in 'tokens'.
                        indexOfNumber += 1;
                        
                        [[patients objectAtIndex:currentPerson] setRoomNumber:[tokens objectAtIndex:indexOfNumber]];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's room number to %@", [[patients objectAtIndex:currentPerson] roomNumber]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        int indexOfRoom = [tokens indexOfObject:@"room"]; //index of the word "room" in 'tokens'.
                        indexOfRoom += 1;
                        
                        [[patients objectAtIndex:currentPerson] setRoomNumber:[tokens objectAtIndex:indexOfRoom]];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set the patient's room number to %@", [[patients objectAtIndex:currentPerson] roomNumber]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                } 
            }
            
            else if(docSwitch == YES)
            {
                if([tokenset containsObject:@"specialty"])
                {
                    int indexOfSpecialty = [tokens indexOfObject:@"specialty"];
                    int indexOfValue     = indexOfSpecialty + 1;
                    
                    [[[physicians objectAtIndex:currentPerson] specialties] addObject:[tokens objectAtIndex:indexOfValue]];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added %@ to Doctor %@'s specialties.", [[[physicians objectAtIndex:currentPerson] specialties] lastObject], [[physicians objectAtIndex:currentPerson] printFirstLast] ]];
                    
                    [ctx sendRequestCompleted];
                    return YES;           
                }
                
                else if([tokenset containsObject:@"sub-specialty"] || [tokenset containsObject:@"subspecialty"] || [tokenset containsObject:@"sub"])
                {
                    int indexOfSubSpecialty;
                    
                    if([tokenset containsObject:@"sub-specialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"sub-specialty"];
                    }
                    
                    else if([tokenset containsObject:@"subspecialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"subspecialty"];
                    }
                    
                    else if([tokenset containsObject:@"specialty"])
                    {
                        indexOfSubSpecialty = [tokens indexOfObject:@"specialty"];
                    }
                    
                    int indexOfValue = indexOfSubSpecialty + 1;
                    int checkVal     = indexOfValue        + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[[physicians objectAtIndex:currentPerson] subSpecialties] addObject:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added %@ to Doctor %@'s sub-specialties.", [[[patients objectAtIndex:currentPerson] subSpecialties] lastObject], [[patients objectAtIndex:currentPerson] printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"accepted"] && [tokenset containsObject:@"insurance"]) || [tokenset containsObject:@"insurance"])
                {
                    int indexOfInsurance;
                    
                    if([tokens containsObject:@"provider"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"provider"];
                    }
                    
                    else if([tokens containsObject:@"insurance"])
                    {
                        indexOfInsurance = [tokens indexOfObject:@"insurance"];
                    }
                    
                    int indexOfValue = indexOfInsurance + 1;
                    int checkVal     = indexOfValue     + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[[physicians objectAtIndex:currentPerson] acceptedInsurance] addObject:[tokens objectAtIndex:indexOfValue]];
                        
                        [ctx sendAddViewsUtteranceView:@"Okay, I've added %@ to Doctor %@'s accepted insurances."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        [[physicians objectAtIndex:currentPerson] setFacilityName:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've added %@ to Doctor %@'s accepted insurances", [[[physicians objectAtIndex:currentPerson] acceptedInsurance] lastObject], [[physicians objectAtIndex:currentPerson] printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;

                    }
                }
                
                else if(([tokenset containsObject:@"hospital"] && [tokenset containsObject:@"affiliation"]) || [tokenset containsObject:@"hospital"] || [tokenset containsObject:@"affiliation"]) 
                {
                    int indexOfHospital;
                    
                    if([tokens containsObject:@"affiliation"])
                    {
                        indexOfHospital = [tokens indexOfObject:@"affiliation"];
                    }
                    
                    else if([tokens containsObject:@"hospital"])
                    {
                        indexOfHospital = [tokens indexOfObject:@"hospital"];
                    }
                    
                    int indexOfValue = indexOfHospital + 1;
                    int checkVal     = indexOfValue    + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setHospitalAffiliation:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s hospital affiliation to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] hospitalAffiliation]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s hospital affiliation to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] hospitalAffiliation]]];
                        }
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[physicians objectAtIndex:currentPerson] setHospitalAffiliation:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s facility name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] hospitalAffiliation]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"facility"] && [tokenset containsObject:@"name"]) || [tokenset containsObject:@"facility"])
                {
                    int indexOfFacility;
                    
                    if([tokens containsObject:@"name"])
                    {
                        indexOfFacility = [tokens indexOfObject:@"name"];
                    }
                    
                    else if([tokens containsObject:@"facility"])
                    {
                        indexOfFacility = [tokens indexOfObject:@"hospital"];
                    }
                    
                    int indexOfValue = indexOfFacility + 1;
                    int checkVal     = indexOfValue    + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setFacilityName:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s facility name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] facilityName]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s facility name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] facilityName]]];
                        }
                            
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        int i = indexOfValue;
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[physicians objectAtIndex:currentPerson] setFacilityName:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s facility name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] facilityName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"office"] && [tokenset containsObject:@"name"]))
                {
                    int indexOfOffice;
                    
                    if([tokens containsObject:@"name"])
                    {
                        indexOfOffice = [tokens indexOfObject:@"name"];
                    }
                    
                    else if([tokens containsObject:@"facility"])
                    {
                        indexOfOffice = [tokens indexOfObject:@"hospital"];
                    }
                    
                    int indexOfValue = indexOfOffice + 1;
                    int checkVal     = indexOfValue  + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setFacilityName:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s office name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] facilityName]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s office name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] facilityName]]];
                        }
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[physicians objectAtIndex:currentPerson] setPhone_emerg:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s office name to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] officeName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"emergency"] && [tokenset containsObject:@"phone"]) || [tokenset containsObject:@"emergency"])
                {
                    int indexOfEmergency;
                    
                    if([tokens containsObject:@"phone"])
                    {
                        indexOfEmergency = [tokens indexOfObject:@"phone"];
                    }
                    
                    else if([tokens containsObject:@"emergency"])
                    {
                        indexOfEmergency = [tokens indexOfObject:@"emergency"];
                    }
                    
                    int indexOfValue = indexOfEmergency + 1;
                    int checkVal     = indexOfValue  + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setPhone_emerg:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s emergency phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_emerg]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s emergency phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_emerg]]];
                        }
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                     
                        [[physicians objectAtIndex:currentPerson] setPhone_emerg:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s emergency phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_emerg]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if(([tokenset containsObject:@"cell"] && [tokenset containsObject:@"phone"]) || [tokenset containsObject:@"cell"])
                {
                    int indexOfCell;
                    
                    if([tokens containsObject:@"phone"])
                    {
                        indexOfCell = [tokens indexOfObject:@"phone"];
                    }
                    
                    else if([tokens containsObject:@"cell"])
                    {
                        indexOfCell = [tokens indexOfObject:@"cell"];
                    }
                    
                    int indexOfValue = indexOfCell   + 1;
                    int checkVal     = indexOfValue  + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setPhone_cell:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s cell phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_cell]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s cell phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_cell]]];
                        }
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[physicians objectAtIndex:currentPerson] setPhone_cell:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s cell phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_cell]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else if([tokenset containsObject:@"office"] && [tokenset containsObject:@"phone"])
                {
                    int indexOfOffice;
                    
                    indexOfOffice = [tokens indexOfObject:@"phone"];
                    
                    int indexOfValue = indexOfOffice + 1;
                    int checkVal     = indexOfValue  + 1;
                    
                    if(checkVal == [tokens count])
                    {
                        [[physicians objectAtIndex:currentPerson] setPhone_office:[tokens objectAtIndex:indexOfValue]];
                        
                        if([tokens containsObject:@"set"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've set Doctor %@'s office phone number %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_office]]];
                        }
                        
                        if([tokens containsObject:@"change"])
                        {
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s office phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_office]]];
                        }
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        NSString* returnString = [[NSString autorelease] init];
                        
                        int i = indexOfValue;
                        
                        while(i < [tokens count])
                        {
                            [returnString stringByAppendingString:[tokens objectAtIndex:i]];
                            [returnString stringByAppendingString:@" "];
                        }
                        
                        [[physicians objectAtIndex:currentPerson] setPhone_office:returnString];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I've changed Doctor %@'s office phone number to %@.", [[physicians objectAtIndex:currentPerson] printFirstLast], [[physicians objectAtIndex:currentPerson] phone_office]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
        
        [ctx sendAddViewsUtteranceView:@"Hmm...I'm sorry, I didn't quite understand what you said...could you try that again?"];
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    /**!  
        Removes objects from various places. (Removes patients and physicians from their respective NSMutableArrays, removes variables inside of patients and physicians, etc.)
     
     */
    if([tokenset containsObject:@"remove"])
    {
        if([tokenset containsObject:@"all"])
        {
            if([tokenset containsObject:@"patients"])
            {
                patients = [[NSMutableArray alloc] init];
            }
            
            else if([tokenset containsObject:@"physicians"] || [tokenset containsObject:@"doctors"])
            {
                physicians = [[NSMutableArray alloc] init];
            }
        }
        
        else if([tokenset containsObject:@"patient"])
        {
            if(([tokens containsObject:@"this"] || [tokens containsObject:@"current"]) && docSwitch == NO)
            {
                Patient* p = [patients objectAtIndex:currentPerson];
                [patients removeObject:p];
            }
            NSString* first;
            NSString* middle;
            NSString* last;
            
            int indexOfPatient = [tokens indexOfObject:@"patient"];
            int indexOfValue   = indexOfPatient + 1;
            
            int count = [tokens count] - 1;
            
            int check = count - indexOfValue;
            
            if(check == 0)
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, but I need at least both a first and last name to process that request."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(check == 1)
            {
                first = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                last  = [tokens objectAtIndex:indexOfValue];
            }
            
            else if(check == 2)
            {
                first = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                middle = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                last = [tokens objectAtIndex:indexOfValue];
            }
            
            else
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Please try saying your command again."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            NSEnumerator* patientEnm = [patients objectEnumerator];
            Patient* p = [[Patient alloc] init];
            
            while(p = [patientEnm nextObject])
            {
                if(![middle isEqual:nil])
                {
                    if([[p fname] isEqualToString:first] && [[p mname] isEqualToString:middle] && [[p lname] isEqualToString:last])
                    {
                        [patients removeObject:p];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed Doctor %@ from 'patients'.", [p printFullName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else
                {
                    if([[p fname] isEqualToString:first] && [[p lname] isEqualToString:last])
                    {
                        [patients removeObject:p];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed Doctor %@ from 'patients'.", [p printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Sorry, I didn't find a patient with the name %@ %@ %@", first, middle, last]];
            
            [ctx requestHasCompleted];
            return YES;
        }
        
        else if([tokenset containsObject:@"physician"] || [tokenset containsObject:@"doctor"])
        {
            if(([tokens containsObject:@"this"] || [tokens containsObject:@"current"]) && docSwitch == YES)
            {
                Physician* doc = [physicians objectAtIndex:currentPerson];
                [physicians removeObject:doc];
            }
            
            NSString* first;
            NSString* middle;
            NSString* last;
            
            int indexOfDoc;
            
            if([tokens containsObject:@"physician"])
            {
                indexOfDoc = [tokens indexOfObject:@"physician"];
            }
               
            else if([tokens containsObject:@"doctor"])
            {
                indexOfDoc = [tokens indexOfObject:@"doctor"];
            }
            
            int indexOfValue   = indexOfDoc + 1;
            
            int count = [tokens count] - 1;
            
            int check = count - indexOfValue;
            
            if(check == 0)
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, but I need at least both a first and last name to process that request."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(check == 1)
            {
                first = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                last  = [tokens objectAtIndex:indexOfValue];
            }
            
            else if(check == 2)
            {
                first = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                middle = [tokens objectAtIndex:indexOfValue];
                
                indexOfValue += 1;
                
                last = [tokens objectAtIndex:indexOfValue];
            }
            
            else
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Please try saying your command again."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            NSEnumerator* docEnm = [physicians objectEnumerator];
            Physician* doc = [[Physician alloc] init];
            while(doc = [docEnm nextObject])
            {
                if(![middle isEqual:nil])
                {
                    if([[doc fname] isEqualToString:first] && [[doc mname] isEqualToString:middle] && [[doc lname] isEqualToString:last])
                    {
                        [physicians removeObject:doc];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed Doctor %@ from 'physicians'.", [doc printFullName]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                else
                {
                    if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                    {
                        [physicians removeObject:doc];
                        
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed Doctor %@ from 'physicians'.", [doc printFirstLast]]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Sorry, I didn't find a Doctor with the name %@ %@ %@", first, middle, last]];
            
            [ctx requestHasCompleted];
            return YES;
        }
        
        else if([tokenset containsObject:@"condition"])
        {
            NSString* conditionName = [[NSString alloc] init];
            int indexOfCondition = [tokens indexOfObject:@"condition"];
            int indexOfFrom = [tokens indexOfObject:@"from"];
            
            int i = indexOfCondition;
            
            while(i < indexOfFrom)
            {
                [conditionName stringByAppendingString:[tokens objectAtIndex:i]];
                
                if(i != indexOfFrom - 1)
                {
                    [conditionName stringByAppendingString:@" "];
                }
                
            }
            
            if([tokenset containsObject:@"from"])
            {
                NSString* first;
                NSString* middle;
                NSString* last;
                
                if([tokens count] - indexOfFrom <= 1)
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Make sure to include the name of the condition to remove and the first and last name (middle name is optional) of the patient who your removing it from."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokens count] - indexOfFrom == 2)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* patientEnm = [patients objectEnumerator];
                    Patient* p = [[Patient alloc] init];
                    
                    while(p = [patientEnm nextObject])
                    {
                        if([[p fname] isEqualToString:first] && [[p lname] isEqualToString:last])
                        {
                            if([[p conditions] containsObject:conditionName])
                            {
                                [[p conditions] removeObject:conditionName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the conditions listed in the patient's file.", conditionName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                            
                            else
                            {
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"No condition found matching %@", conditionName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
                
                else if([tokens count] - indexOfFrom == 3)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    middle = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* patientEnm = [patients objectEnumerator];
                    Patient* p = [[Patient alloc] init];
                    
                    while(p = [patientEnm nextObject])
                    {
                        if([[p fname] isEqualToString:first] && [[p mname] isEqualToString:middle] && [[p lname] isEqualToString:last])
                        {
                            if([[p conditions] containsObject:conditionName])
                            {
                                [[p conditions] removeObject:conditionName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the conditions listed in the patient's file.", conditionName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                            
                            else
                            {
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"No condition found matching %@", conditionName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
            }
            
            else
            {
                if([[[patients objectAtIndex:currentPerson] conditions] containsObject:conditionName])
                {
                    [[[patients objectAtIndex:currentPerson] subSpecialties] removeObject:conditionName];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the patient's file.", conditionName]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, there wasn't a condiiton with that name in the patient's file."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
        }
        
        else if([tokenset containsObject:@"insurance"])
        {
            
            NSString* insuranceName = [[NSString alloc] init];
            
            int indexOfInsurance;
            
            if([tokens containsObject:@"provider"])
            {
                indexOfInsurance = [tokens indexOfObject:@"provider"];
            }
            
            else
            {
                indexOfInsurance = [tokens indexOfObject:@"condition"];
            }

            if([tokenset containsObject:@"from"])
            {
                NSString* first;
                NSString* middle;
                NSString* last;
                
                int indexOfFrom = [tokens indexOfObject:@"from"];
                
                int i = indexOfInsurance;
                while(i < indexOfFrom)
                {
                    [insuranceName stringByAppendingString:[tokens objectAtIndex:i]];
                    
                    if(i != indexOfFrom - 1)
                    {
                        [insuranceName stringByAppendingString:@" "];
                    }
                    
                }
                
                if([tokens count] - indexOfFrom <= 1)
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Make sure to include the name of the condition to remove and the first and last name (middle name is optional) of the patient who your removing it from."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokens count] - indexOfFrom == 2)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc acceptedInsurance] containsObject:insuranceName])
                            {
                                [[doc acceptedInsurance] removeObject:insuranceName];
                            }
                        }
                    }
                }
                
                else if([tokens count] - indexOfFrom == 3)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    middle = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc acceptedInsurance] containsObject:insuranceName])
                            {
                                [[doc acceptedInsurance] removeObject:insuranceName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the patient's file.", insuranceName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
            }
            
            else
            {
                if([[[physicians objectAtIndex:currentPerson] acceptedInsurance] containsObject:insuranceName])
                {
                    [[[physicians objectAtIndex:currentPerson] subSpecialties] removeObject:insuranceName];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the patient's file.", insuranceName]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, there wasn't a sub-specialty with that name in the doctor's file."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
        }
        
        else if([tokenset containsObject:@"specialty"])
        {
            NSString* specialtyName = [[NSString alloc] init];
            
            int indexOfSpecialty = [tokens indexOfObject:@"specialty"];
            
            if([tokenset containsObject:@"from"])
            {
                NSString* first;
                NSString* middle;
                NSString* last;
                
                
                
                int indexOfFrom = [tokens indexOfObject:@"from"];
                
                int i = indexOfSpecialty;
                while(i < indexOfFrom)
                {
                    [specialtyName stringByAppendingString:[tokens objectAtIndex:i]];
                    
                    if(i != indexOfFrom - 1)
                    {
                        [specialtyName stringByAppendingString:@" "];
                    }
                    
                }
                
                if([tokens count] - indexOfFrom <= 1)
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Make sure to include the name of the condition to remove and the first and last name (middle name is optional) of the patient who your removing it from."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokens count] - indexOfFrom == 2)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc specialties] containsObject:specialtyName])
                            {
                                [[doc specialties] removeObject:specialtyName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the specialties listed in the doctor's file.", specialtyName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
                
                else if([tokens count] - indexOfFrom == 3)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    middle = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc specialties] containsObject:specialtyName])
                            {
                                [[doc specialties] removeObject:specialtyName];
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the specialties listed in the doctor's file.", specialtyName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
            }
            
            else
            {
                if([[[physicians objectAtIndex:currentPerson] specialties] containsObject:specialtyName])
                {
                    [[[physicians objectAtIndex:currentPerson] specialties] removeObject:specialtyName];
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the specialties listed in the doctor's file.", specialtyName]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, there wasn't a specialty with that name in the doctor's file."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
        }
        
        else if([tokenset containsObject:@"sub-specialty"] || [tokenset containsObject:@"subspecialty"] || [tokenset containsObject:@"sub"])
        {
            NSString* subSpecialtyName = [[NSString alloc] init];
            int indexOfSubSpecialty;
            if([tokens containsObject:@"sub-specialty"])
            {
                indexOfSubSpecialty = [tokens indexOfObject:@"sub-specialty"];
            }
            
            else if([tokens containsObject:@"subspecialty"])
            {
                indexOfSubSpecialty = [tokens indexOfObject:@"subspecialty"];
            }
            
            else if([tokens containsObject:@"specialty"])
            {
                indexOfSubSpecialty = [tokens indexOfObject:@"specialty"];
            }
            
            if([tokenset containsObject:@"from"])
            {
                NSString* first;
                NSString* middle;
                NSString* last;
                
                
                
                int indexOfFrom = [tokens indexOfObject:@"from"];
                
                int i = indexOfSubSpecialty;
                while(i < indexOfFrom)
                {
                    [subSpecialtyName stringByAppendingString:[tokens objectAtIndex:i]];
                    
                    if(i != indexOfFrom - 1)
                    {
                        [subSpecialtyName stringByAppendingString:@" "];
                    }
                    
                }
                
                if([tokens count] - indexOfFrom <= 1)
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite understand what you said. Make sure to include the name of the condition to remove and the first and last name (middle name is optional) of the patient who your removing it from."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else if([tokens count] - indexOfFrom == 2)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc specialties] containsObject:subSpecialtyName])
                            {
                                [[doc subSpecialties] removeObject:subSpecialtyName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the sub-specialties listed in the doctor's file.", subSpecialtyName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
                
                else if([tokens count] - indexOfFrom == 3)
                {
                    indexOfFrom += 1;
                    
                    first = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    middle = [tokens objectAtIndex:indexOfFrom];
                    
                    indexOfFrom += 1;
                    
                    last  = [tokens objectAtIndex:indexOfFrom];
                    
                    NSEnumerator* docEnm = [patients objectEnumerator];
                    Physician* doc = [[Physician alloc] init];
                    
                    while(doc = [docEnm nextObject])
                    {
                        if([[doc fname] isEqualToString:first] && [[doc lname] isEqualToString:last])
                        {
                            if([[doc subSpecialties] containsObject:subSpecialtyName])
                            {
                                [[doc subSpecialties] removeObject:subSpecialtyName];
                                
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the sub-specialties listed in the doctor's file.", subSpecialtyName]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                        }
                    }
                }
            }
            
            else
            {
                if([[[physicians objectAtIndex:currentPerson] subSpecialties] containsObject:subSpecialtyName])
                {
                    [[[physicians objectAtIndex:currentPerson] subSpecialties] removeObject:subSpecialtyName];
                    
                    [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Okay, I removed %@ from the sub-specialties listed in the doctor's file.", subSpecialtyName]];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
                
                else
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, there wasn't a sub-specialty with that name in the doctor's file."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
        }
    }
    
    else if([tokenset containsObject:@"list"]) /**> Lists all Patient objects within 'patients' and Physician objects within 'physicians' */
    {
        if([patients count] == 0 && [physicians count] == 0)
        {
            [ctx sendAddViewsUtteranceView:@"There aren't any patient or doctor files currently in the system."];
            [
             ctx sendRequestCompleted];
            return YES;
        }
        
        if([patients count] == 0)
        {
            [ctx sendAddViewsUtteranceView:@"No patient files in the system."];
        }
        
        else
        {
            NSEnumerator* patientEnm = [patients objectEnumerator];
            Patient* p = [[Patient alloc] init];
            [ctx sendAddViewsUtteranceView:@"List of Patients:"];
            
            while(p = [patientEnm nextObject])
            {
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@",[p printAll]]];
            }
            
            [ctx sendAddViewsUtteranceView:@"End of Patient List"];
        }
        
        if([physicians count] == 0)
        {
            [ctx sendAddViewsUtteranceView:@"No physician files in the system."];
        }
        
        else
        {
            NSEnumerator* docEnm = [physicians objectEnumerator];
            Physician* doc = [[Physician alloc] init];
            
            [ctx sendAddViewsUtteranceView:@"List of Doctors:"];
            
            while(doc = [docEnm nextObject])
            {
                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@",[doc printAll]]];
            }
            
            [ctx sendAddViewsUtteranceView:@"End of Doctor List"];
            
            [ctx sendRequestCompleted];
            return YES;
        }
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    else if([tokenset containsObject:@"show"] || [tokenset containsObject:@"see"] || [tokenset containsObject:@"view"] || [tokenset containsObject:@"display"]) /**> One-call to "Display" a specific Patient or Physician using the user's input.*/
    {
        if([tokenset containsObject:@"the"] && (([tokenset containsObject:@"patient"] || [tokenset containsObject:@"patient's"] || [tokenset containsObject:@"patients"]) || ([tokenset containsObject:@"doctor"] || [tokenset containsObject:@"doctor's"] || [tokenset containsObject:@"doctors"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"physician's"] || [tokenset containsObject:@"physicians"])))
        {
            if(([tokenset containsObject:@"doctor"] || [tokenset containsObject:@"doctor's"] || [tokenset containsObject:@"doctors"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"physician's"] || [tokenset containsObject:@"physicians"]) && docSwitch == YES)
            {
                [[physicians objectAtIndex:currentPerson] printAll];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else if(docSwitch == NO && ([tokenset containsObject:@"patient"] || [tokenset containsObject:@"patient's"] || [tokenset containsObject:@"patients"]))
            {
                [[patients objectAtIndex:currentPerson] printAll];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
            else
            {
                [ctx sendAddViewsUtteranceView:@"Error. You might have tried to display a loaded doctor's file when a patient file is currently loaded..."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
            
        }
        
        else if([tokenset containsObject:@"patient"] && ([tokenset containsObject:@"file"] || [tokenset containsObject:@"records"] || [tokenset containsObject:@"record"]))
        {
            
            int indexOfPatient = [tokens indexOfObject:@"patient"];
            int indexOfFile;
            
            if([tokenset containsObject:@"file"])
            {
                indexOfFile = [tokens indexOfObject:@"file"];
            }
               
            else if([tokenset containsObject:@"records"])
            {
                indexOfFile = [tokens indexOfObject:@"records"];
            }
            
            else if([tokenset containsObject:@"record"])
            {
                indexOfFile = [tokens indexOfObject:@"record"];
            }
            
            if(indexOfPatient < indexOfFile)
            {
                int patient = indexOfPatient++;
                int names =  indexOfFile - patient; //patient is one more than indexOfPatient to get correct number of "names" (combo of first, middle, last)
                
                if(names == 2)
                {
                    NSString* first = [tokens objectAtIndex:patient];
                    NSString* last  = [tokens objectAtIndex:patient+1];
                    NSString* newLast;
                    
                    int length = [last length];
                    
                    if([last characterAtIndex:length-1] == '\'')
                    {
                        NSLog(@"\' detected");
                        newLast = [last substringToIndex:length-1];
                        NSLog(@"newLast is: %@", newLast);
                    }
                    
                    NSEnumerator* patientEnm = [patients objectEnumerator];
                    Patient* p = [[Patient alloc] init];
                    while(p = [patientEnm nextObject])
                    {
                        if(newLast != nil)
                        {
                            if([[p fname] isEqualToString:first]  && [[p lname] isEqualToString:newLast])
                            {
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [p printAll]]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                            
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [p printAll]]];
                           
                            [ctx sendRequestCompleted];
                            return YES;
                        }
                    }
                    
                    [ctx sendAddViewsUtteranceView:@"I didn't find any patients matching that name in the database"];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
        
                else if(names == 3)
                {
                    NSString* first  = [tokens objectAtIndex:patient];
                    NSString* middle = [tokens objectAtIndex:patient+1];
                    NSString* last   = [tokens objectAtIndex:patient+2];
                    NSString* newLast = nil;
                    
                    int length = [last length];
                    
                    if([last characterAtIndex:length-1] == '\'')
                    {
                        newLast = [last substringToIndex:length-1];
                    }
                    
                    NSEnumerator* patientEnm = [patients objectEnumerator];
                    Patient* p = [[Patient alloc] init];
                    while(p = [patientEnm nextObject])
                    {
                        if(newLast != nil)
                        {
                            if([[p fname] isEqualToString:first] && [[p mname] isEqualToString:middle] && [[p lname] isEqualToString:newLast])
                            {
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [p printAll]]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                            
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [p printAll]]];
                            
                            [ctx sendRequestCompleted];
                            return YES;
                        }
                    }
                    
                    [ctx sendAddViewsUtteranceView:@"I didn't find any patients matching that name in the database"];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }

                else
                {
                    [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite get that. Try asking for the patient's file like this \"show patient John Doe's file\"."];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
        }
        
        else if(([tokenset containsObject:@"doctor"] || [tokenset containsObject:@"doctor's"] || [tokenset containsObject:@"doctors"] || [tokenset containsObject:@"physician"] || [tokenset containsObject:@"physician's"] || [tokenset containsObject:@"physicians"]) && ([tokenset containsObject:@"file"] || [tokenset containsObject:@"records"] || [tokenset containsObject:@"record"]))
        {
            int indexOfDoc;
            
            if([tokenset containsObject:@"doctor"])
            {
                indexOfDoc = [tokens indexOfObject:@"doctor"];
            }
            
            else if([tokenset containsObject:@"doctor's"])
            {
                indexOfDoc = [tokens indexOfObject:@"doctor's"];
            }
            
            else if([tokenset containsObject:@"doctors"])
            {
                indexOfDoc = [tokens indexOfObject:@"doctors"];
            }
            
            else if([tokenset containsObject:@"physician"])
            {
                indexOfDoc = [tokens indexOfObject:@"physician"];
            }
            
            else if([tokenset containsObject:@"physician's"])
            {
                indexOfDoc = [tokens indexOfObject:@"physician's"];
            }
            
            else if([tokenset containsObject:@"physicians"])
            {
                indexOfDoc = [tokens indexOfObject:@"physicians"];
            }
             
            int indexOfFile;
            
            if([tokenset containsObject:@"file"])
            {
                indexOfFile = [tokens indexOfObject:@"file"];
            }
            
            else if([tokenset containsObject:@"records"])
            {
                indexOfFile = [tokens indexOfObject:@"records"];
            }
            
            else if([tokenset containsObject:@"record"])
            {
                indexOfFile = [tokens indexOfObject:@"record"];
            }
            
            if(indexOfDoc < indexOfFile)
            {
                int doc = indexOfDoc - 1;
                
                int numOfNames =  indexOfFile - doc; //patient is one more than indexOfPatient to get correct number of "names" (combo of first, middle, last)

                if(numOfNames == 2)
                {
                    NSString* first = [tokens objectAtIndex:doc];
                    NSString* last  = [tokens objectAtIndex:doc+1];
                    NSString* newLast;
                    
                    int length = [last length];
                    
                    if([last characterAtIndex:length-1] == '\'')
                    {
                        NSLog(@"\' detected");
                        newLast = [last substringToIndex:length-1];
                        NSLog(@"newLast is: %@", newLast);
                    }
                    
                    NSEnumerator* docEnm = [physicians objectEnumerator];
                    Physician* doctor = [[Physician alloc] init];
                    while(doctor = [docEnm nextObject])
                    {
                        if(newLast != nil)
                        {
                            if([[doctor fname] isEqualToString:first]  && [[doctor lname] isEqualToString:newLast])
                            {
                                [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [doctor printAll]]];
                                
                                [ctx sendRequestCompleted];
                                return YES;
                            }
                            
                            [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"%@", [doctor printAll]]];
                            
                            [ctx sendRequestCompleted];
                            return YES;
                        }
                    }
                    
                    [ctx sendAddViewsUtteranceView:@"I didn't find any doctors matching that name in the database"];
                    
                    [ctx sendRequestCompleted];
                    return YES;
                }
            }
                
            else
            {
                [ctx sendAddViewsUtteranceView:@"Sorry, I didn't quite get that. Try asking for the doctor's file like this \"show doctor John Doe's file\"."];
                
                [ctx sendRequestCompleted];
                return YES;
            }
        }


        else if([tokenset containsObject:@"patient"] && !([tokenset containsObject:@"file"] || [tokenset containsObject:@"files"] || [tokenset containsObject:@"record"] || [tokenset containsObject:@"records"]))
        {
            int indexOfPatient = [tokens indexOfObject:@"patient"];
            int numberOfNames  = [tokens count] - indexOfPatient;
            
            if(numberOfNames == 2)
            {
                int firstLoc = indexOfPatient += 1;
                int lastLoc  = indexOfPatient += 2;
                
                NSString* first = [tokens objectAtIndex:firstLoc];
                NSString* last  = [tokens objectAtIndex: lastLoc];
                
                NSEnumerator* patientEnm = [patients objectEnumerator];
                Patient* p = [[Patient alloc] init];
                
                while(p = [patientEnm nextObject])
                {
                    if([[p fname] isEqualToString:first] && [[p lname] isEqualToString:last])
                    {
                        [p printAll];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            //Assume user wants to view patient file
            else if(numberOfNames == 3)
            {
                int firstLoc  = indexOfPatient += 1;
                int middleLoc = indexOfPatient += 2;
                int lastLoc   = indexOfPatient += 3;
                
                NSString* first  = [tokens objectAtIndex: firstLoc];
                NSString* middle = [tokens objectAtIndex:middleLoc]; 
                NSString* last   = [tokens objectAtIndex:  lastLoc];
                
                NSEnumerator* patientEnm = [patients objectEnumerator];
                Patient* p = [[Patient alloc] init];
                
                while(p = [patientEnm nextObject])
                {
                    if([[p fname] isEqualToString:first] && [[p mname] isEqualToString:middle] && [[p lname] isEqualToString:last])
                    {
                        [p printAll];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
    }

    else if(([tokenset containsObject:@"what"] && [tokenset containsObject:@"is"]) || [tokenset containsObject:@"what's"])
    {
        if([tokenset containsObject:@"what"] && [tokenset containsObject:@"is"])
        {
            if([tokenset containsObject:@"the"] && [tokenset containsObject:@"patient's"])
            {
                if([tokenset containsObject:@"age"])
                {
                    if([[[patients objectAtIndex:currentPerson] age] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's age."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's age is %@", [[patients objectAtIndex:currentPerson] age] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                if([tokenset containsObject:@"weight"])
                {
                    
                    if([[[patients objectAtIndex:currentPerson] weight] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's weight."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's weight is %@", [[patients objectAtIndex:currentPerson] weight] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                if([tokenset containsObject:@"height"])
                {
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }

                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's height is %@", [[patients objectAtIndex:currentPerson] height] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                if([tokenset containsObject:@"room"])
                {
                    if([[[patients objectAtIndex:currentPerson] roomNumber] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's room number."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's room number is %@", [[patients objectAtIndex:currentPerson] roomNumber] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            else if([tokenset containsObject:@"patient"] || [tokenset containsObject:@"patient's"])
            {
                if([tokenset containsObject:@"age"])
                {
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's height is %@", [[patients objectAtIndex:currentPerson] age] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                if([tokenset containsObject:@"weight"])
                {
                    
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's weight."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's weight is %@", [[patients objectAtIndex:currentPerson] weight] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            
                if([tokenset containsObject:@"height"])
                {
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's height is %@", [[patients objectAtIndex:currentPerson] height] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
                
                if([tokenset containsObject:@"room"])
                {
                    if([[[patients objectAtIndex:currentPerson] roomNumber] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's room number."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's room number is %@", [[patients objectAtIndex:currentPerson] roomNumber] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
        
        else if([tokenset containsObject:@"what's"])
        {
            if([tokenset containsObject:@"the"] && ([tokenset containsObject:@"patient's"] || [tokenset containsObject:@"patients"]))
            {
                if([tokenset containsObject:@"age"])
                {
                    if([[[patients objectAtIndex:currentPerson] age] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's age is %@", [[patients objectAtIndex:currentPerson] age] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"weight"])
                {
                    
                    if([[[patients objectAtIndex:currentPerson] weight] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's weight."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's weight is %@", [[patients objectAtIndex:currentPerson] weight] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"height"])
                {
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's height is %@", [[patients objectAtIndex:currentPerson] height] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"room"])
                {
                    if([[[patients objectAtIndex:currentPerson] roomNumber] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's room number."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's room number is %@", [[patients objectAtIndex:currentPerson] roomNumber] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
            
            else if([tokenset containsObject:@"patient"] || [tokenset containsObject:@"patient's"] || [tokenset containsObject:@"patients"])
            {
                if([tokenset containsObject:@"age"])
                {
                    if([[[patients objectAtIndex:currentPerson] age] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's age."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's age is %@", [[patients objectAtIndex:currentPerson] age] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"weight"])
                {
                    
                    if([[[patients objectAtIndex:currentPerson] weight] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's weight."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's weight is %@", [[patients objectAtIndex:currentPerson] weight] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"height"])
                {
                    if([[[patients objectAtIndex:currentPerson] height] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's height."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's height is %@", [[patients objectAtIndex:currentPerson] height] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                }
                
                if([tokenset containsObject:@"room"])
                {
                    if([[[patients objectAtIndex:currentPerson] roomNumber] isEqualToString:@""])
                    {
                        [ctx sendAddViewsUtteranceView:@"Currently, there isn't a value set for the patient's room number."];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                    
                    else
                    {
                        [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"The patient's room number is %@", [[patients objectAtIndex:currentPerson] roomNumber] ]];
                        
                        [ctx sendRequestCompleted];
                        return YES;
                    }
                }
            }
        }
    }
    
    
    
    else if([tokenset containsObject:@"save"]) /** */
                                                
    {
        
        NSString * path = [self pathForDataFile];
        
        NSMutableDictionary * rootObject;
        rootObject = [NSMutableDictionary dictionary];
        
        [rootObject setValue: [self patients] forKey:@"patients"];
        [NSKeyedArchiver archiveRootObject: rootObject toFile: path];
        
        [ctx sendAddViewsUtteranceView:@"Okay, I've saved the patient list."];
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    else if([tokenset containsObject:@"load"])
    {
        NSString* path = [self pathForDataFile];
        NSDictionary* rootObject;
        
        rootObject = [NSKeyedUnarchiver unarchiveObjectWithFile:path];    
        [self setPatients: [rootObject valueForKey:@"patients"]];
            
        [ctx sendAddViewsUtteranceView:@"Okay, I've loaded the patient list."];
        
        [ctx sendRequestCompleted];
        return YES;
    }
    
    else if([tokenset containsObject:@"help"])
    {
        if(!asked)
        {
            asked = YES;
            SOObject* askView = [ctx createAssistantUtteranceView:@"Are you okay? Should I call a nurse?"];
            [[askView objectForKey:@"properties"] setObject:[NSNumber numberWithBool:YES] forKey:@"listenAfterSpeaking"];
            NSArray* views = [NSArray arrayWithObject:askView];
            
            [ctx sendAddViews:views];
            
            [ctx sendRequestCompleted];
            return YES;
        }
    }
    
    if(asked)
    {
        asked = NO;
        
        if([tokenset containsObject:@"hurt"] || [tokenset containsObject:@"pain"] || [tokenset containsObject:@"hurt"] || [tokenset containsObject:@"hurts"] || [tokenset containsObject:@"hurting"] || [tokenset containsObject:@"help"] || [[tokens objectAtIndex:0] isEqualToString:@""])
        {
               [ctx sendAddViewsUtteranceView:@"Try to stay calm, someone's on their way to help."];
               [ctx sendAddViewsUtteranceView:[NSString stringWithFormat:@"Code Red: Room %@, repeat Code Red: Room %@", [[patients objectAtIndex:currentPerson] roomNumber], [[patients objectAtIndex:currentPerson] roomNumber]]];
               
               [ctx sendRequestCompleted];
               return YES;
        }
    }
   
    return NO;
}


@end
