//
//  MedHistory.h
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Physician.h"
#import "Address.h"

@interface MedHistory : NSObject

@property (nonatomic, retain) NSString* eventType;
@property (nonatomic, retain) NSString* eventSubType;

@property (nonatomic, retain) NSDate*   eventDate;

@property (nonatomic, retain) Physician* physician;

@property (nonatomic, retain) NSString* hospitalName;
@property (nonatomic, retain) Address* hospitalAddress;

-(id)init;

@end
