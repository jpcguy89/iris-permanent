//
//  Address.h
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject

@property (nonatomic, retain) NSString*     city;
@property (nonatomic, retain) NSString*  country;
@property (nonatomic, retain) NSString* houseNum;
@property (nonatomic, retain) NSString*    state;
@property (nonatomic, retain) NSString*   street;
@property (nonatomic, retain) NSString*  zipcode;

-(id)init;
-(id)initWithFullAddress:(NSString*)newCity country:(NSString*)newCountry houseNum:(NSString*)newHouseNum state:(NSString*)newState street:(NSString*)newStreet zipcode:(NSString*)newZipcode;
-(id)initWithHouseStreetZipCityState:(NSString*)newCity houseNum:(NSString*)newHouseNum state:(NSString*)newState street:(NSString*)newStreet zipcode:(NSString*)newZipcode;
-(id)initWithCityStateZip:(NSString*)newCity state:(NSString*)newState zipcode:(NSString*)newZipcode;
-(id)initWithHouseStreetZip:(NSString*)newHouseNum street:(NSString*)newStreet zipcode:(NSString*)newZipcode;
-(id)initWithHouseStreet:(NSString*)newHouseNum street:(NSString*)newStreet;



-(NSString*)print;

@end
