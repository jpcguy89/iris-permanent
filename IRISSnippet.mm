//
//  IRISSnippet.m
//  IRISSnippet
//
//  Created by  on 12/18/11.
//  Copyright (c) 2011 .me. All rights reserved.
//

#import "IRISSnippet.h"
#import "IRISCommands.h"
#import <Foundation/Foundation.h>

@implementation IRISSnippet

- (id)view
{
    //NSLog(@">> IRISSnippetController view");
    return _view;
}

- (void)dealloc
{
    NSLog(@">> IRISSnippet dealloc");
    [_view release];
	[super dealloc];
}

- (id)initWithProperties:(NSDictionary*)props;
{
	NSLog(@">> IRISSnippet initWithProperties: Properties: %@", props);

    if ( (self = [super init]) )
    {
        // here we load a view from a nib file
        if (![[NSBundle bundleForClass:[self class]] loadNibNamed:@"IRISNib" owner:self options:nil])
        {
            NSLog(@"Warning! Could not load nib file.\n");
            return NO;
        }
        _view = [_IRISNib retain]; 
        [_IRISLabel setText:[props objectForKey:@"text"]]; // text from IRISCommands
        
        // ...but you are free to do GUI programatically
        //UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        //[lbl setText:@"IRIS Snippet!"];
        //_view = lbl;
    }
    return self;
}

@end



// -------------------

@interface IRIS : NSObject<SEExtension> 
@end

@implementation IRIS

// required initialization
-(id)initWithSystem:(id<SESystem>)system
{
    if ( (self = [super init]) )
    {
        [system registerCommand:[IRISCommands class]];
        [system registerSnippet:[IRISSnippet class]];
    }
    return self;
}

// optional info about extension
-(NSString*)author
{
    return @"jpcguy89";
}
-(NSString*)name
{
    return @"IRISSnippet";
}
-(NSString*)description
{
    return @"An example of assistant extension.";
}
-(NSString*)website
{
    return @"N/A";
}

@end
