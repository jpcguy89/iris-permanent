//
//  Patient.h
//  IRIS
//
//  Created by Joseph Cavallaro on 3/15/12.
//  Copyright (c) 2012. All rights reserved.
//

/*! 
 *  \brief     Patient Object Class
 *  \details   Creates Patient objects and Stores relevant medical information for them.
 *  \author    Joe Cavallaro
 *  \date      March-April 2012
 *  \copyright Joseph Peter Cavallaro 2012
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Patient : NSObject <NSCoding>


@property (nonatomic, strong) NSString* fname;
@property (nonatomic, strong) NSString* mname;
@property (nonatomic, strong) NSString* lname;
@property (nonatomic, strong) NSString* age;
@property (nonatomic, strong) NSString* height;
@property (nonatomic, strong) NSString* weight;
@property (nonatomic, strong) NSString* roomNumber;

@property (nonatomic, strong) NSMutableArray* conditions;
@property (nonatomic, strong) NSMutableArray* history;


- (id)initWithFullName :(NSString*)first middleName:(NSString*) middle lastName:(NSString*) last; //@details Initialzes a Patient variable with a user defined first, middle, and last name. The rest of the variables are set to @"" (empty string).
- (id)initWithFirstLast:(NSString*)first   lastName:(NSString*) last;//@details Initialzes a Patient variable with a user defined first and last name. The rest of the variables are set to @"" (empty string).
- (id)initWithFirst    :(NSString*)first;//@details Initialzes a Patient variable with a user defined first name. The rest of the variables are set to @"" (empty string).
- (id)initWithLast     :(NSString*)last;//@details Initialzes a Patient variable with a user defined last name. The rest of the variables are set to @"" (empty string).
- (id)init;//@details Initialzes a Patient object and sets all variables to @"" (empty string)
- (id)clear;//@details Sets all Patient variables to @"" (emprt string).

- (NSString*)printAll;//@details Returns a NSString containing all information stored for the Patient.
- (NSString*)printFirstLast;//@details Returns an NSString containing the first and last name of the Patient.
- (NSString*)printFullName;//@details Returns an NSString containing the first, middle, and last name of the patient.
- (NSString*)printConditions;//@details Returns an NSString containing all medical conditions stored for the Patient.

//NSCoding Required Protocol Methods
- (void)encodeWithCoder:(NSCoder *)aCoder;//@details Encodes the data to be saved and loaded for later use. 
- (id)initWithCoder:(NSCoder *)aDecoder;//@details Initializes a new Patient object by decoding the saved encoding of a Patient object.

@end
