//
//  HelloCommands.mm
//  HelloSnippet
//
//  Created by K3A on 12/29/11.
//  Copyright (c) 2011 K3A.me. All rights reserved.
//

#import "HelloCommands.h"


@implementation K3AHelloCommands

-(void)dealloc
{
	NSLog(@"K3AHelloCommands dealloc");
	[super dealloc];
}

-(BOOL)handleSpeech:(NSString*)text tokens:(NSArray*)tokens tokenSet:(NSSet*)tokenset context:(id<SEContext>)ctx
{
	NSLog(@">> K3AHelloCommands handleSpeech %@", text);
    
    	// reacts to only one token - "test" 
	if ([tokenset count] == 1 && [tokenset containsObject:@"test"])
	{
        	// properties for the snippet
        	NSDictionary* snipProps = [NSDictionary dictionaryWithObject:@"Text passed as a snippet property." forKey:@"text"];
        
        	// create an array of views
        	NSMutableArray* views = [NSMutableArray arrayWithCapacity:1];
        	[views addObject:[ctx createAssistantUtteranceView:@"Hello Snippet!!"]];
		[views addObject:[ctx createSnippet:@"K3AHelloSnippet" properties:snipProps]];

        	// send views to the assistant
        	[ctx sendAddViews:views];

        	// alternatively, for utterance response, you can use this call only:
		//[ctx sendAddViewsAssistantUtteranceView:@"Hello Snippet!!"];
		// alternatively, for snippet response you can use this call only:
		//[ctx sendAddViewsSnippet:@"K3AHelloSnippet" properties:snipProps];

		// inform the assistant that this is end of the request
		// you can spawn an additional thread and process request asynchronly, ending with sending "request completed"
		[ctx sendRequestCompleted];

		return YES; // inform the system that the command has been handled (ignore the original one from the server)
	}

	return NO;	
}

@end
