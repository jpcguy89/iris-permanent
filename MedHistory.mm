//
//  MedHistory.mm
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MedHistory.h"

@implementation MedHistory



@synthesize eventType;
@synthesize eventSubType;

@synthesize eventDate;

@synthesize physician;
@synthesize hospitalName;
@synthesize hospitalAddress;

-(id)init
{
    self = [super init];
    
    if(self)
    {
        eventDate       = [[NSDate    alloc] init];
        physician       = [[Physician alloc] init];
        hospitalAddress = [[Address   alloc] init];
        
        hospitalName = @"";
        eventType    = @"";
        eventSubType = @"";
        
    }
    
    return self;
}
@end
