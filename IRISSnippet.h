//
//  IRISSnippet.h
//  IRIS
//
//  Created by Joseph Cavallaro on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SiriObjects.h"

@interface IRISSnippet : NSObject <SESnippet> {

    UIView* _view;
    IBOutlet UILabel* _IRISLabel;
    IBOutlet UILabel* _IRISNib;
}


@end
