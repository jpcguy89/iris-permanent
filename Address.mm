//
//  Address.m
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Address.h"

@implementation Address

@synthesize     city;
@synthesize    state;
@synthesize  country;

@synthesize houseNum;
@synthesize   street;
@synthesize  zipcode;


-(id)init
{
    self = [super init];
    
    if(self)
    {
        city     =    @"";
        country  = @"USA";
        houseNum =    @"";
        state    =    @"";
        street   =    @"";
        zipcode  =    @"";
    }
    
    return self;
        
}

-(id)initWithFullAddress:(NSString *)newCity country:(NSString *)newCountry houseNum:(NSString *)newHouseNum state:(NSString *)newState street:(NSString *)newStreet zipcode:(NSString *)newZipcode
{
    self = [super init];
    
    if(self)
    {
        [self init];
        
        houseNum = newHouseNum;
        street   = newStreet;
        zipcode  = newZipcode;
        
        city     = newCity;
        state    = newState;
        country  = newCountry;
    }
    
    return self; 
    
}

-(id)initWithHouseStreetZipCityState:(NSString *)newCity houseNum:(NSString *)newHouseNum state:(NSString *)newState street:(NSString *)newStreet zipcode:(NSString *)newZipcode
{
    self = [super init];
    
    if(self)
    {
        [self init];
        
        houseNum = newHouseNum;
          street = newStreet;
         zipcode = newZipcode;
        
           city  = newCity;
          state  = newState;
        country  = @"USA";
    }
    
    return self;
}

-(id)initWithHouseStreetZip:(NSString *)newHouseNum street:(NSString *)newStreet zipcode:(NSString *)newZipcode
{
    self = [super init];
    
    if(self)
    {
        [self init];
        
        houseNum = newHouseNum;
          street = newStreet;
         zipcode = newZipcode;
        
           city  =    @"";
          state  =    @"";
        country  = @"USA";
    }
    
    return self;
}

-(id)initWithCityStateZip:(NSString *)newCity state:(NSString *)newState zipcode:(NSString *)newZipcode
{
    self = [super init];
    
    if(self)
    {
        [self init];
        
        houseNum = @"";
          street = @"";
         zipcode = newZipcode;
        
           city  = newCity;
          state  = newState;
        country  = @"USA";
    }
    
    return self;
}

-(id)initWithHouseStreet:(NSString *)newHouseNum street:(NSString *)newStreet
{
    self = [super init];
    
    if(self)
    {
        [self init];
        
        houseNum = newHouseNum;
          street = newStreet;
         zipcode = @"";
        
           city  = @"";
          state  = @"";
        country  = @"USA";
    }
    
    return self;
}





-(NSString*)print
{
    NSString* address = [NSString stringWithFormat:@"%@ %@ %@, %@ %@ %@", houseNum, street, city, state, zipcode, country];
    
    return address;
}


@end
