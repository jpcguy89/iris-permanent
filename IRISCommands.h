//
//  IRISCommands.h
//  IRISSnippet
//
//  Created by  on 12/29/11.
//  Copyright (c) 2011 .me. All rights reserved.
//
/** 
    @brief     The central hub for all actions and processing of commands.
    @details   Processes all user given commands, stores data, searches stored data, gives verbal responses containing information and prompts.
    @author    Joe Cavallaro
    @date      March-April 2012
    @copyright Joseph Peter Cavallaro 2012
 
*/

#import <Foundation/Foundation.h>
#import "SiriObjects.h"
#import "Patient.h"

@interface IRISCommands : NSObject <SECommand, NSCoding>

@property (nonatomic, assign) int currentPerson; /**< Holds the index of the currently loaded Patient or Physician. */

@property (nonatomic, retain) NSMutableArray* patients; /**< Holds all created Patient objects. */
@property (nonatomic, retain) NSMutableArray* physicians; /**< Holds all created Physician objects. */
@property (nonatomic, retain) NSArray* allConditions; /**< A pre-defined list of all valid medical conditions. */
@property (nonatomic, retain) NSArray* allSpecialties; /**< A pre-defined list of all valid specialties (for Physicians) */


-(BOOL)handleSpeech:(NSString*)text tokens:(NSArray*)tokens tokenSet:(NSSet*)tokenset context:(id<SEContext>)ctx; /**< Handles all commands. */
-(void)encodeWithCoder:(NSCoder*) encoder; //*< Encodes and saves important data to be loaded later on. */
-(id)initWithCoder:(NSCoder*) decoder; //* Decodes, loads, and initializes all data/variables that were encoded and saved previously.*/

@end 
