//
//  Physician.m
//  IRIS
//
//  Created by Joseph Cavallaro on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Physician.h"

@implementation Physician


@synthesize fname;
@synthesize mname;
@synthesize lname;

@synthesize specialties;
@synthesize subSpecialties; 
@synthesize acceptedInsurance;

//@synthesize facilityAddress;
//@synthesize officeAddress;    // Addresses causing crashes, eliminating them for now.


@synthesize officeName;
@synthesize facilityName;
@synthesize hospitalAffiliation;

@synthesize phone_cell;
@synthesize phone_emerg;
@synthesize phone_office;



- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:fname               forKey:@"fname"              ];
    [coder encodeObject:mname               forKey:@"mname"              ];
    [coder encodeObject:lname               forKey:@"lname"              ];
    [coder encodeObject:specialties         forKey:@"specialties"        ];
    [coder encodeObject:acceptedInsurance   forKey:@"acceptedInsurance"  ];
    [coder encodeObject:hospitalAffiliation forKey:@"hospitalAffiliation"];
    [coder encodeObject:phone_cell          forKey:@"phone_cell"         ];
    [coder encodeObject:phone_emerg         forKey:@"phone_emerg"        ];
    [coder encodeObject:phone_office        forKey:@"phone_office"       ];
    
    //[coder encodeObject:officeAddress       forKey:@"officeAddress"      ];
    //[coder encodeObject:facilityAddress     forKey:@"facilityAddress"    ];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    [self init];
    
    self.fname               = [decoder decodeObjectForKey:@"fname"              ];
    self.mname               = [decoder decodeObjectForKey:@"mname"              ];
    self.lname               = [decoder decodeObjectForKey:@"lname"              ];
    self.specialties         = [decoder decodeObjectForKey:@"specialties"        ];
    self.subSpecialties      = [decoder decodeObjectForKey:@"subSpecialties"     ];
    self.acceptedInsurance   = [decoder decodeObjectForKey:@"acceptedInsurance"  ];
    self.hospitalAffiliation = [decoder decodeObjectForKey:@"hospitalAffiliation"];
    self.phone_cell          = [decoder decodeObjectForKey:@"phone_cell"         ];
    self.phone_emerg         = [decoder decodeObjectForKey:@"phone_emerg"        ];
    self.phone_office        = [decoder decodeObjectForKey:@"phone_office"       ];
    
    //self.officeAddress       = [decoder decodeObjectForKey:@"officeAddress"      ];
    //self.facilityAddress     = [decoder decodeObjectForKey:@"facilityAddress"    ];
    
    return self;
}

-(id)initWithFullName:(NSString *)first middleName:(NSString*)middle lastName:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
        [self init];
        fname = first;
        mname = middle;
        lname = last;
    }
    
    return self;
}



-(id)initWithFirstLast:(NSString *)first lastName:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
        fname  = first;
        lname  = last;
        mname  = @"";
        lname  = @"";
    }
    
    return self;
}

-(id)initWithFirst:(NSString *)first
{
    self = [super init];
    
    if(self)
    {
        [self init];
        fname = first;
    }
    
    return self;
}

-(id)initWithLast:(NSString *)last
{
    self = [super init];
    
    if(self)
    {
        [self init];
        lname = last;
    }
    
    return self;
}

-(id)clear
{
    fname = @"";
    mname = @"";
    lname = @"";
    
    return self;
}

-(id)init
{
    self = [super init];
    
    if(self)
    {
        fname = @"";
        mname = @"";
        lname = @"";
        
        specialties         = [[NSMutableArray alloc] init];
        subSpecialties      = [[NSMutableArray alloc] init];
        acceptedInsurance = [[NSMutableArray alloc] init];
        
        officeName = @"";
        facilityName = @"";
        hospitalAffiliation = @"";
        
        phone_cell   = @"";
        phone_emerg  = @"";
        phone_office = @"";
        
        //officeAddress = [[Address alloc] init];
        //facilityAddress = [[Address alloc] init];
    }
    
    return self;
}
-(NSString*)printFullName
{
    return [NSString stringWithFormat:@"%@ %@ %@", self.fname ,self.mname ,self.lname];
}

-(NSString*)printFirstLast
{
    return [NSString stringWithFormat:@"%@ %@", self.fname ,self.lname];
}
-(NSString*)printAll
{
    NSMutableString* print = [NSMutableString stringWithFormat:@"Name: %@\n", [self printFullName]];
    if([self.specialties count] == 0)
    {
        [print appendString:@"Specialties: are not found in the doctor's file.\n"];
    }
    else
    { 
        [print appendFormat:@"Specialties: %@\n", [self printSpecialties]];
    }
    if([self.subSpecialties count] == 0)
    {
        [print appendString:@"Sub-Specialties: not found in the doctor's file.\n"];
    }
    else
    { 
        [print appendFormat:@"Sub-Specialties: %@\n", [self printSpecialties]];
    }
    if([self.acceptedInsurance count] == 0)
    {
        [print appendString:@"Accepted Insurances: are not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Accepted Insurances: %@\n", [self printAcceptedInsurances]]; 
    }
    if([officeName isEqualToString:@""])
    {
        [print appendString:@"Office Name: not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Office Name: %@",self.officeName];
    }
    if([facilityName isEqualToString:@""])
    {
        [print appendString:@"Facility Name: not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Facility Name: %@\n", facilityName];
    }
    if([hospitalAffiliation isEqualToString:@""])
    {
        [print appendString:@"Hospital Affiliation: not found in the doctor's file.\n"];
    }
    else 
    {
        [print appendFormat:@"Hospital Affiliaiton %@\n", hospitalAffiliation];
    }
    if([phone_cell isEqualToString:@""])
    {
        [print appendString:@"Cell Phone Number: not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Cell Phone Number: %@\b", self.phone_cell];
    }
    
    if([phone_emerg isEqualToString:@""])
    {
        [print appendString:@"Emergency Phone Number: not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Emergency Phone Number: %@\b", self.phone_emerg];
    }
    if([phone_office isEqualToString:@""])
    {
        [print appendString:@"Office Phone Number: not found in the doctor's file.\n"];
    }
    else
    {
        [print appendFormat:@"Office Phone Number: %@", self.phone_office];
    }
    
    return print;
    
}
-(NSString*)printSpecialties
{
    return [[specialties valueForKey:@"description"] componentsJoinedByString:@"\n"];
}

-(NSString*)printSubSpecialties
{
    return [[subSpecialties valueForKey:@"description"] componentsJoinedByString:@"\n"];
}

-(NSString*)printAcceptedInsurances
{
    return [[acceptedInsurance valueForKey:@"description"] componentsJoinedByString:@"\n"];
}
@end
